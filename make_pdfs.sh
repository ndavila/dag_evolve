if [ "$#" -ne 1 ]; then
    echo "Usage: $0 directory"
    echo "  directory = directory with input dot files and location for output pdfs"
    exit
fi

for i in $1/*.dot
do
  [ -f "$i" ] || break
  dot -Tpdf $i -o ${i%.dot}.pdf
done

dot -Tpdf $1/output.dot -o $1/output.pdf

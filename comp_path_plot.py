import numpy as np
import matplotlib.pyplot as plt
x = np.genfromtxt('origin')
y = np.genfromtxt('cd_inc')
z = np.genfromtxt('cd_dec')

def annot_max(x,y, ax=None):
    xmax = x[np.argmax(y)]
    ymax = y.max()
    text= "x={:.3f}, y={:.3f}".format(xmax, ymax)
    if not ax:
        ax=plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    arrowprops=dict(arrowstyle="->",connectionstyle="angle,angleA=0,angleB=60")
    kw = dict(xycoords='data',textcoords="axes fraction",
              arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
    ax.annotate(text, xy=(xmax, ymax), xytext=(0.94,0.96), **kw)

#annot_max(x[:,0],x[:,2])

k = 1
plt.plot( x[:,0], x[:,2*k], label='Path ' + str(k) + '_original')
plt.plot( y[:,0], y[:,2*k], label='Path ' + str(k) + '_cd_increase')


plt.xlabel('$\\tau$')
plt.ylabel('$G(\\tau)$')
plt.legend()
plt.savefig('out.pdf')

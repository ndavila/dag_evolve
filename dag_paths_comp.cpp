#include <iostream>

#include <boost/graph/directed_graph.hpp>
#include <boost/graph/iteration_macros.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>

#include "helper.hpp"
#include "utility/dag_paths.hpp"
#include "utility/all_paths_finder.hpp"

using namespace std;
using namespace boost;

// Declare the graph type and its vertex and edge types.

typedef directed_graph<
  property<vertex_index_t, size_t, property<vertex_name_t, std::string > >,
  property<edge_weight_t, double, property<edge_color_t, std::string> > > Graph;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef std::map<std::string, Vertex> VertexMap;

int
main(int argc, char *argv[])
{
    if( argc != 5 )
    {
      std::cerr << "Wrong inputs." << std::endl;
      exit( EXIT_FAILURE );
    }

    // Create the graph and read it from standard input.

    Graph g;

    typedef typename boost::property_map<Graph, boost::edge_weight_t>::type
      WeightMap;
    typedef typename boost::property_traits<WeightMap>::value_type weight_t;

    WeightMap  wm = get(edge_weight, g);

    typename property_map <Graph, edge_color_t>::type
      cm = get(edge_color, g);
    typename property_map <Graph, vertex_name_t>::type
      nm = get(vertex_name, g);

    std::ifstream input_file( argv[1] );

    if( !input_file.good() )
    {
      std::cerr << "Invalid input." << std::endl;
      exit( EXIT_FAILURE );
    }

    VertexMap verts = read_weighted_graph(g, nm, wm, input_file);
    input_file.close();

    wn_user::utility::dag_checker<Graph> my_checker;

    if( !my_checker( g ) )
    {
      std::cerr << "Graph is not acyclic." << std::endl;
      exit( EXIT_FAILURE );
    }

    // Write out the graph.

    std::string s = std::string( argv[4] );
    std::ofstream my_output( (s + "/output.dot").c_str() );

    if( !my_output.good() )
    {
      std::cerr << "Invalid output." << std::endl;
      exit( EXIT_FAILURE );
    }

    write_graphviz(
      my_output, g, vertexWriter<Graph>( g ), edgeWriter<Graph>( g ),
      graphWriter( "" )
    );

    my_output.close();

    // Find the paths.

    wn_user::utility::all_paths_finder<Graph, Vertex> my_finder;

    std::vector<std::vector<Vertex> > v_p =
      my_finder( verts[argv[2]], verts[argv[3]], g );

    // Create the path evolvers.

    std::vector<wn_user::utility::dag_path_evolver<Graph, Vertex> > p;
    std::vector<wn_user::utility::dag_path_expansion_evolver<Graph, Vertex> > pb;

    for( size_t i = 0; i < v_p.size(); i++ )
    {
      pb.push_back(
        wn_user::utility::dag_path_expansion_evolver<Graph, Vertex>( g, v_p[i], 20 )
      );
      p.push_back(
        wn_user::utility::dag_path_evolver<Graph, Vertex>( g, v_p[i] )
      );
    }

    // Evolve the paths.  Use both the integrator and the Bell expansion.

    for( size_t j = 0; j < 500; j++ )
    {
      weight_t tau = 0.01 * j;
      std::cout << tau;
      for( size_t k = 0; k < p.size(); k++ )
      {
        std::cout << "  " << p[k](tau);
        std::cout << "  " << pb[k](tau);
      }
      std::cout << std::endl;
    }

    // Loop on paths to output diagnostics.
    
    std::ofstream my_output2( (s + "/diagnostics.txt").c_str() );


    for( size_t i = 0; i < v_p.size(); i++ )
    {
       my_output2 << boost::format{"Path %d\n\n"} % (i+1);
       my_output2 << boost::format{"Alpha = %f  Mean = %f  Alpha_max = %f\n\n"}
                       % pb[i].getAlpha() % pb[i].getMean()
                       % pb[i].getAlphaMax();

       my_output2 << "Moments:" << std::endl << std::endl;
       my_output2 << boost::format{"Path factor = %f\n\n"}
                       % pb[i].getPathFactor();
       std::vector<double> mu = pb[i].getMoments(10);
       my_output2 << "i" << "\t" << "  mu[i]\n\n";
       for( size_t j = 0; j < mu.size(); j++ )
       {
         my_output2 << boost::format{"%d\t  %.2e\n"} % j % mu[j];
       }
       my_output2 << std::endl;
       
       my_output2 << "Bell polynomial diagnositics:" << std::endl << std::endl;

       std::vector<double> b = pb[i].getB();
       std::vector<double> b_scaled = pb[i].getScaledB();
       std::vector<double> x = pb[i].getX();
       std::vector<double> x_scaled = pb[i].getScaledX();

       my_output2 <<
         boost::format{"i\t b[i]\t\t b_scaled[i]\t x[i]\t\t x_scaled[i]\n\n"};

       for( size_t j = 0; j < x.size(); j++ )
       {
         my_output2 << boost::format{"%d\t %.2e\t %.2e\t %.2e\t %.2e\n"} %
                         j % b[j] % b_scaled[j] % x[j] % x_scaled[j];
       }

       my_output2 << std::endl;
    }

    my_output2.close();

    // Loop on paths to output graphs.

    for( size_t i = 0; i < v_p.size(); i++ )
    {
      // Reset the edge colors.

      BGL_FORALL_EDGES(e, g, Graph)
      {
        cm[e] = "black";
      }

      Edge e;
      bool found;

      for( size_t j = 0; j < v_p[i].size() - 1; j++ )
      {
        boost::tie(e, found) = edge(v_p[i][j], v_p[i][j+1], g);
        cm[e] = "red";
      }

      // Write out the graph for this path.

      std::string s_path = boost::lexical_cast<std::string>( i + 1 );

      std::ofstream my_output( (s + "/output_" + s_path + ".dot").c_str() );

      write_graphviz(
        my_output, g, vertexWriter<Graph>( g ), edgeWriter<Graph>( g ),
        graphWriter( "Path " + s_path )
      );

      my_output.close();

    }

    return 0;
}

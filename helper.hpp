#include <boost/graph/graphviz.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost;

// The vertex writer.

template<typename Graph>
class vertexWriter
{
  public:
    vertexWriter(){}
    vertexWriter( Graph& g )
    {
      vertexNameMap = boost::get( vertex_name, g );
    };

    template <class Vertex>
    void operator()( std::ostream& out, const Vertex& v ) const
    {
      out <<
          format(
              "[label=\"%s\"]"
          ) %
              vertexNameMap[v]
          <<
          std::endl;
    } 

  private:
    typename property_map<Graph, vertex_name_t>::type
        vertexNameMap;

};

// The edge writer.

template<typename Graph>
class edgeWriter
{
  public:
    edgeWriter(){}
    edgeWriter( Graph& g )
    {
      edgeWeightMap = boost::get( edge_weight, g );
      edgeColorMap = boost::get( edge_color, g );
    };

    template <class Edge>
    void operator()( std::ostream& out, const Edge& e ) const
    {
      out <<
          format(
              "[label = \"%.1f\" color = \"%s\" fontcolor = \"%s\"]"
          ) %
              edgeWeightMap[e] % edgeColorMap[e] % edgeColorMap[e]
          <<
          std::endl;
    } 

  private:
    typename property_map<Graph, edge_weight_t>::type
        edgeWeightMap;
    typename property_map <Graph, edge_color_t>::type
        edgeColorMap;

};

// The labelled graph writer.

class graphWriter
{
  public:
    graphWriter( std::string _s ) : s( _s ){}

    void operator()( std::ostream& out ) const
    {
      out << "rankdir=LR;" << std::endl;
      out << "labelloc = \"t\";" << std::endl;
      out << "label = \"" << s << "\";" << std::endl;
    } 

  private:
    std::string s;
};

// Function to add a named vertex.

template <typename Graph, typename NameMap, typename VertexMap>
typename boost::graph_traits<Graph>::vertex_descriptor
add_named_vertex(Graph& g, NameMap nm, const std::string& name, VertexMap& vm)
{
    typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
    typedef typename VertexMap::iterator Iterator;

    Vertex v;
    Iterator iter;
    bool inserted;
    boost::tie(iter, inserted) = vm.insert(make_pair(name, Vertex()));
    if(inserted) {
        // The name was unique so we need to add a vertex to the graph
        v = add_vertex(g);
        iter->second = v;
        put(nm, v, name);      // store the name in the name map
    }
    else {
        // We had alread inserted this name so we can return the
        // associated vertex.
        v = iter->second;
    }
    return v;
}

// Read in weighted graph.

template <typename Graph, typename NameMap, typename WeightMap, typename InputStream>
std::map<std::string, typename boost::graph_traits<Graph>::vertex_descriptor>
read_weighted_graph(Graph& g, NameMap nm, WeightMap wm, InputStream& is)
{
    typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
    typedef typename boost::graph_traits<Graph>::edge_descriptor Edge;
    std::map<std::string, Vertex> verts;
    for(std::string line; std::getline(is, line); ) {
        if(line.empty()) break;
        std::size_t i = line.find_first_of(',');
        std::size_t j = line.find_first_of(',', i + 1);
        std::string first(line, 0, i);
        std::string second(line, i + 1, j - i - 1);
        std::string prob(line, j + 1);

        boost::algorithm::trim( first );
        boost::algorithm::trim( second );
        boost::algorithm::trim( prob );

        std::stringstream ss(prob);
        float p;
        ss >> p;

        // add the vertices to the graph
        Vertex u = add_named_vertex(g, nm, first, verts);
        Vertex v = add_named_vertex(g, nm, second, verts);

        // add the edge and set the weight
        Edge e = add_edge(u, v, g).first;
        put(wm, e, p);
    }
    return verts;
}

